#!/usr/bin/python

import time
import threading
from struct import pack

class Telemetry:
	
	def __init__(self, ser, vesc, state_estimator):
		self.ser = ser
		self.vesc = vesc
		self.state_estimator = state_estimator
		
	def start(self):
		tx = Telemetry_tx(self.ser, self.state_estimator)
		rx = Telemetry_rx(self.ser, self.vesc)
		tx.start()
		rx.start()

class Telemetry_rx(threading.Thread):

	def __init__(self, ser, vesc):
		threading.Thread.__init__(self)
		self.cmd = 's'
		self.ser = ser
		self.vesc = vesc

	def run(self):
		while True:
			cmd = str(self.ser.read(1), 'ascii')
			if cmd is not 'g':
				self.vesc.go_ok = False
			else:
				self.vesc.go_ok = True

class Telemetry_tx(threading.Thread):

	def __init__(self, ser, state_estimator):
		threading.Thread.__init__(self)
		self.ser = ser
		self.state_estimator = state_estimator

	def run(self):
		while True:
			vel = self.state_estimator.vel
			heading = self.state_estimator.heading
			latitude = self.state_estimator.gnss_pos[0]
			longitude = self.state_estimator.gnss_pos[1]
			altitude= self.state_estimator.gnss_pos[2]
			data = pack('fffff', vel, heading, latitude, longitude, altitude)
			self.ser.write(data)
			time.sleep(0.05)
