#!/usr/bin/python

import sys
import serial
import threading
import time
from struct import unpack
from serial.tools import list_ports

class Telemetry_tx(threading.Thread):

	def __init__(self, ser):
		threading.Thread.__init__(self)
		self.ser = ser
		self.cmd = 's'

	def set_cmd(self, new_cmd):
		self.cmd = new_cmd

	def run(self):
		while True:
			self.ser.write(bytes(self.cmd, 'ascii'))
			time.sleep(0.05)

class KeyboardListener(threading.Thread):

	def __init__(self, tlm_tx):
		threading.Thread.__init__(self)
		self.tlm_tx = tlm_tx

	def run(self):
		while True:
			cmd = input("Input cmd ('g' for GO, or 's' for STOP): ")
			self.tlm_tx.set_cmd(cmd)

class Telemetry_rx(threading.Thread):

	def __init__(self, ser, log):
		threading.Thread.__init__(self)
		self.log = log
		self.ser = ser
		self.t0 = time.time()
		self.data = 0

	def run(self):
		self.ser.reset_input_buffer()
		while True:
			data = self.ser.read(20)
			if len(data) == 20:
				state = unpack('fffff', data)
				log.write(str(time.time()-self.t0))
				log.write(',')
				for value in state:
					log.write(str(value))
					log.write(',')
				log.write('\n')
			else:
				self.ser.reset_input_buffer()

class XbeeCommData:
	vid = 1027
	pid = 24597

if __name__ == "__main__":

	# Log telemetry data
	filename_tag = "remote_log"
	filename = 'log.csv'#'logs/' + time.strftime('%c').replace(" ", "_").replace(":", "-") + "_" + filename_tag + ".csv"
	log = open(filename, "w")
	log.write("time, velocity, heading, latitude, longitude, altitude")
	log.write('\n')

	# Find serial port
	ser_name = 'None'
	for port in list_ports.comports(include_links=True):
		if (port.vid == XbeeCommData.vid and port.pid == XbeeCommData.pid):
			ser_name = port.device
	if ser_name is 'None':
		print("Could not find Xbee serialport")
		sys.exit()
	
	serialport = serial.Serial(ser_name, 115200, timeout=0.25)
	time.sleep(0.01)

	# Start threads
	tlm_tx = Telemetry_tx(serialport)
	tlm_tx.daemon = True
	tlm_tx.start()
	
	kb_listener = KeyboardListener(tlm_tx)
	kb_listener.daemon = True
	kb_listener.start()

	tlm_rx = Telemetry_rx(serialport, log)
	tlm_rx.daemon = True
	tlm_rx.start()

	while True:
		time.sleep(0.5)
