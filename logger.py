
import time
import threading

class Logger(threading.Thread):

	def __init__(self, state_estimator, controller, filename):
		threading.Thread.__init__(self)
		self.state_estimator = state_estimator
		self.controller      = controller

		# Set up the filename
		try:
			self.filename    = str(filename)
		except:
			print("Could not convert filename to string, using default name")
			self.filename = "log_file"
		self.filename = time.strftime('%c').replace(" ", "_") + "_" + self.filename + ".csv"


	def run(self):
		log = open("logs/"+self.filename, "w+")
		log.write("time, velocity, heading, latitude, longitude, altitude")
		log.write('\n')
		time0 = time.time()
		while True:
			log.write(str(time.time()-time0))
			log.write(',')
			log.write(str(self.state_estimator.vel))
			log.write(',')
			log.write(str(self.state_estimator.heading))
			log.write(',')
			log.write(str(self.state_estimator.gnss_pos[0]))
			log.write(',')
			log.write(str(self.state_estimator.gnss_pos[1]))
			log.write(',')
			log.write(str(self.state_estimator.gnss_pos[2]))
			log.write('\n')
			time.sleep(0.05)
