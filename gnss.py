
import threading
import time
from pyUBX import UBXManager
from pyUBX import UBXMessage
import state_estimator

local_pos = []

class GNSSHandler:
        
    def __init__(self, ser_gnss, state_estimator):
        self.ser = ser_gnss
        self.state_estimator = state_estimator
        self.running = True

    def local_pos_callback(local_pos):
        self.state_estimator.report_local_pos(local_pos)
        
    def set_refresh_rate(self):
        self.ser.reset_input_buffer()
        self.ser.reset_output_buffer()
        rate_msg = b'\xB5\x62\x06\x08\x06\x00\x64\x00\x01\x00\x01\x00\x7A\x12'
        self.ser.write(rate_msg)
        time.sleep(0.2)
        
    def start_gnss(self):
        #self.set_refresh_rate()
        manager = UBXManager.UBXManager(self.ser, self.state_estimator)
        manager.start()
