
import serial
import math
import time
import threading

class StateEstimator:
    
    def __init__(self):
        self.state = [0.0, 0.0, 0.0, 0.0, 0.0]
        self.gnss_pos = [0.0, 0.0, 0.0]
        self.vel = 0.0
        self.heading = 0.0
    
    # Function called from GNSS handler
    def report_local_pos(self, new_gnss_pos):
        self.gnss_pos = new_gnss_pos
    
    # Function called from VESC handler
    def report_vel(self, new_vel):
        self.vel = new_vel
        
    def get_state(self):
        self.state = [self.vel, self.heading, self.gnss_pos[0], self.gnss_pos[1], self.gnss_pos[2]]
        return self.state
                    
            