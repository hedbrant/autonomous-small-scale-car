# Autonomous Small-Scale Car
This autonomous RC-car setup was created as part of a master's thesis at the Department of Automatic Control in the spring of 2020. The intent of this platform is to make it easy to create different vehicle control systems and navigation methods on an easily programmable Python-platform run on a Raspberry Pi computer. Similar platforms exist, such as F1-tenth, but with the drawback of being based on Robot Operating System (ROS) which, at the time of writing, does not support Python 3. Since many peripheral modules on this car, such as the VESC motor speed controller and the GNSS-receivers, use open source Python libraries that rely on Python 3, there is a need for an easily usable platform that supports it.

## Hardware Setup
The hardware on the car consists of 4 modules connected to a Raspberry Pi through USB. The modules in the system are the following.
* A GNSS receiver (u-blox ZED-F9P) and a complementary antenna.
* A compass - an Arduino microcontroller that takes magnetometer readings from a separate device and returns a heading to the Pi.
* An Xbee RF communication device which makes it possible to communicate with a remote Xbee device connected to a separate computer for control and logging.
* A VESC motor speed controller which controls the motor velocity and position of the steering servo.

The Pi is powered by a power bank and the VESC and in turn the motor and steering servo is controlled by a separate 7 cell NiMH or a 3 cell LiPo battery.

## Software Setup
### On the Car
The code is divided into several different modules for different purposes.

* **main** - Initiates everything and starts all threads. Also finds the right COM port for every external USB module.
* **state_estimator** - Stores the state information and similar data.
* **gnss** - Continuously receives data from the GNSS receiver and updates the position state in the state_estimator module.
* **compass** - Continuously reads the heading and updates it in the state_estimator module.
* **controller** - Performs the control algorithm based on the state_estimator, then sends the output to the vesc module.
* **vesc** - Communicates the desired velocity and steering position to the motor speed controller.
* **logger** - Stores data in a CSV-file that can be viewed after a run.
* **telemetry** - Communicates data in real time to the Xbee module for remote data analysis. Also reads stop or go commands from a remote computer connected to another Xbee module.

### On External Computer 
Using an external computer connected to the separate Xbee device, real-time control and logging can be performed. To do this, the telemetry/**ext_ctrl_telemetry.py** module can be run. 

### Dependencies
The code depends on a number of Python libraries.

* **PySerial** is used to communicate over USB.
* **PyVESC** (https://github.com/LiamBindle/PyVESC) is used to handle the communication with the VESC motor speed controller.
* A customized version of the **PyUBX** library (already in the repo) is used to communicate with the GNSS receiver and to interpret the received data.

