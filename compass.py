#!/usr/bin/python

import serial
import math
import time
import threading

class Compass(threading.Thread):
    
    def __init__(self, ser, state_estimator):
        threading.Thread.__init__(self)
        self.ser = ser
        self.state_estimator = state_estimator
        
    def run(self):
        heading = 0.0
        self.ser.reset_input_buffer()
        while True:
            heading_raw = self.ser.read(2)
            if len(heading_raw) > 0:
                heading = heading_raw[0] * 2.0 + heading_raw[1] * 0.00793650793651
                heading = heading - 15.0                # Offset because of physical chip mounting
                if heading < 0.0:
                    heading = heading + 360.0
                if heading > 360.0:
                    heading = heading - 360.0
            self.state_estimator.heading = heading
