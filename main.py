#!/usr/bin/python
import sys
import serial
from serial import Serial
from serial.tools import list_ports
import time

import gnss
from gnss import GNSSHandler
from state_estimator import StateEstimator
from controller import Controller
from vesc import VescHandler
from compass import Compass
from telemetry import Telemetry
from logger import Logger

################################################################################
# Starting the main loop on boot
################################################################################
# To run the main loop on boot, I would advice cd to
# the autonomous-small-scale-car on the raspberry pi, and
# run an 'ls -la'. Note that none of the files are executable.
# Now, make the main file executable by writing 'chmod +x main.py'.
# The main file should now be executable (highlighted green), and
# you should be able to cd able to cd to the home directoory (just
# type cd). Here you will find a hidden file, which you can open with
# nano .bashrc. At the very end of this file, you could add a line as
# if you launch the main file from the terminal with an absolute path
# i.e., 'sudo python3 /home/pi/XX/main.py'. To take effect, reboot the
# car by hand or using 'sudo reboot'. Note that you may want to include a
# time delay in the launch of threads and call to list_ports.


################################################################################
# Idea for finding ports and writing the prinf statements to trace in /logs
################################################################################

# (vid, pid, USB product string)
HARDWARE = {
    'vesc': (1155, 22336, 'ChibiOS/RT Virtual COM Port'),
    'gnss': (5446, 425, 'u-blox GNSS receiver'),
    'comp': (6790, 29987, 'USB2.0-Serial'),
    'xbee': (1027, 24597, 'FT231X USB UART')
}

# This will hold the devie name/path, i.e. /dev/ttyUSB0
PORTS = {
    'vesc': 'none',
    'gnss': 'none',
    'comp': 'none',
    'xbee': 'none'
}

# Direct sys.stdout to the tracefile
def startup():
    original_stdout = sys.stdout
    tracefile = open('logs/trace.txt', 'w')
    sys.stdout = tracefile
    return original_stdout, tracefile

# Safely exit the program
def teardown(original_stdout, tracefile):
    # TODO Write a zero-control signal to the process
    tracefile.close()
    sys.stdout = original_stdout
    sys.exit()

if __name__ == "__main__":

    # Set up the print function to write to trace file
    original_stdout, tracefile = startup()

    # Iterate over the availabel ports looking for matching hardware
    for port in list_ports.comports(include_links=True):
        # Each port is now a "ListPortInfo" object, containing fields
        #
        # device        - Full device name/path, e.g. /dev/ttyUSB0
        # vid           - USB Vendor ID (integer, 0…65535).
        # pid           - USB product ID (integer, 0…65535).
        # serial_number - USB serial number as a string.
        # location      - USB device location string .
        # manufacturer  - USB manufacturer string.
        # product       - USB product string.
        for item in HARDWARE:
            if HARDWARE[item]==(port.vid, port.pid, port.product):
                PORTS[item]=port.device

    # Check that everything is plugged in
    if any([PORTS[key]=='none' for key in PORTS.keys()]):
        print('Could not find all of the hardware items, aborting main loop.')
        teardown(original_stdout, tracefile)

    # Set up the serial ports
    ser_vesc = PORTS['vesc']
    ser_gnss = serial.Serial(PORTS['gnss'], 9600, timeout=None)
    ser_comp = serial.Serial(PORTS['comp'], 115200, timeout=0.5)
    ser_xbee = serial.Serial(PORTS['xbee'], 115200, timeout=0.25)
    time.sleep(0.2)

    
    try:
        state_estimator = StateEstimator()
        vesc = VescHandler(ser_vesc, state_estimator)
        controller = Controller(state_estimator, vesc)
        gnss = GNSSHandler(ser_gnss, state_estimator)
        compass = Compass(ser_comp, state_estimator)
        logger = Logger(state_estimator, controller, "my_logfile")
        telemetry = Telemetry(ser_xbee, vesc, state_estimator)

        gnss.start_gnss()
        compass.start()
        vesc.start()
        controller.start()
        telemetry.start()
        logger.start()
    except:
        print('Could not start the threads')
