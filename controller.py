
import math
import time
import threading
from vesc import VescHandler

ALPHA = 0.7
UB_STEER = 1.0
LB_STEER = 0.0
K_P = 0.02

class Controller(threading.Thread):
    
    def __init__(self, state_estimator, vesc):
        threading.Thread.__init__(self)
        self.state_estimator = state_estimator
        self.vesc = vesc
        self.output = 0.0
        self.output_sat = 0.0
        
        # For logger
        self.alpha = ALPHA
        self.k_p = K_P
        
    def run(self):
        output_prev = self.vesc.steer_mid_pos
        while True:
            vel = self.state_estimator.vel
            heading = self.state_estimator.heading
            gnss_pos = self.state_estimator.gnss_pos
            
            # Steering - P control
            ref_heading = 180.0
            self.output = K_P * (ref_heading - heading)
            output = self.vesc.steer_mid_pos + self.output
            output = ((1.0 - ALPHA) * output) + (ALPHA * output_prev)
            output_prev = output
            if output > UB_STEER:
                output_sat = UB_STEER
            elif output < LB_STEER:
                output_sat = LB_STEER
            else:
                output_sat = output
            self.output_sat = output_sat
            self.vesc.steer_cmd = output_sat
            
            # Velocity - constant velocity
            vel = 1.0
            self.vesc.vel_cmd = vel
            
            time.sleep(0.05)
